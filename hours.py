def effectiveWage(hours, baseSalary):
	noOTSalary = hours / 40 * baseSalary
	baseRate = baseSalary / (40*52)
	averageRate = baseRate + (((hours % 40) / 40) * baseRate * 1.5)
	effOTHrs = 40 + (hours - 40) * 1.5 

	print("Hours: {} | Base Salary: {}\n".format(hours, baseSalary))
	print("Base Wage at 40 (all) hours: {}".format(baseSalary / 2080))
	print("Effective Wage at {} hours: {}".format(hours, baseSalary / (hours * 52)))
	print("Effective Wage (OT hrs considered) at {} (effective) hours: {}".format(effOTHrs, baseSalary / (effOTHrs * 52)))
	print("Average wage at {} hours: {}".format(hours, averageRate))
	effectiveSalary(baseRate, hours)



def effectiveSalary(rate, hours):
	print("Rate: {} | Hours a week: {}\n".format(rate, hours))
	print("Base Salary at 40 hours: {}".format(rate * 40 * 52))
	print("No OT Salary at {} hours: {}".format(hours, hours * rate * 52))
	print("OT Salary at {} hours: {}".format(hours, (40 * rate + hours % 40 * rate * 1.5) * 52))


def main():
	effectiveWage(43, 65000)
	print("\n----------\n")
	effectiveSalary(29.13, 41)


if __name__ == "__main__":
    main()